     % Performs DFT-based analysis (PSD,TOLf (fast 1/3-octave method),Broadband)
% for PAMGuide.m

% This code accompanies the manuscript: 

%   Merchant et al. (2015). Measuring Acoustic Habitats. Methods in Ecology
%    and Evolution

% and follows the equations presented in Appendix S1. It is not necessarily
% optimised for efficiency or concision.

% Copyright � 2014 The Authors.

% Author: Nathan D. Merchant. Last modified 22 Sep 2014

%modified by Jason Wood April 2016
%I removed the shortening of the audio file when last point is zero as I
%needed to zero padd the short beluga audio files (lines 63-65)

function [APSD,ABROAD,A10_100,A100_1000,A1000_10000,A10000_100000,A13,f]...
    = PG_DFTJW_GUI(yy,Fs,S,N,r,winname,envi,lcut,hcut,atype,tstamp,disppar)


% Inputs
% yy - soundfile data
% Fs - sample rate in Hz
% N - window length
% S - end to end calibration constant
% r- dft advance (%)
% winname - type of window (e.g. hannint)
% env - enviornment bool for reference pressure 'air', 'water' for air/water
% lcut - low frequency cutoff
% hcut - hight frequency circus
% atype - type of analysis run
% tstamp -?? 
% disappar -??



if disppar == 1
switch atype                  
    case 'PSD'                      %if PSD selected
        fprintf('Computing PSD...')
    case 'PowerSpec'                %if power spectrum selected
        fprintf('Computing power spectrum...')
    case 'Broadband'                %if broadband level selected
        fprintf('Computing broadband level...')
    case 'TOL'                     %if TOL selected
        fprintf('Computing 1/3-octave levels...')
    case 'All'                      %if all analyses selected
        fprintf('Computing all levels...')
        
end
end
tic

switch envi
    case 'Air'
    pref = 20;
    case 'Wat'
    pref = 1;
end


%% COMPUTING POWER SPECTRUM ---------------------------------------

%% Divide signal into data segments (corresponds to EQUATION 5)

xl = length(yy);

if N > xl                           %check segment is shorter than file
    disp('Error: The chosen segment length is longer than the file.')
    A = 0;
    return
end
yy = single(yy);                %reduce precision to single for speed

%grid whose rows are each (overlapped) segment for analysis
xgrid = buffer(yy,N,ceil(N*r),'nodelay').';    
                                    
                              
clear xbit
%if xgrid(length(xgrid(:,1)),N) == 0 %remove final segment if not full
%    xgrid = xgrid(1:length(xgrid(:,1))-1,:);
%end

M = length(xgrid(:,1));             %total number of data segments


%% Apply window function (corresponds to EQUATION 6)

switch winname                      %define window
    case 'None'                     %i.e. rectangular (Dirichlet) window
        w = ones(1,N);
        alpha = 1;                  %scaling factor
    case 'Hann'                     %Hann window         
        w = (0.5 - 0.5*cos(2*pi*(1:N)/N));
        alpha = 0.5;                %scaling factor
    case 'Hamming'                  %Hamming window
        w = (0.54 - 0.46*cos(2*pi*(1:N)/N));
        alpha = 0.54;               %scaling factor
    case 'Blackman'                 %Blackman window
        w = (0.42 - 0.5*cos(2*pi*(1:N)/N) + 0.08*cos(4*pi*(1:N)/N));
        alpha = 0.42;               %scaling factor
end

xgrid = xgrid.*repmat(w/alpha,M,1);
                                    %multiply segments by window function

%% Compute DFT (EQUATION 7)

X = abs(fft(xgrid.')).';            %calculate DFT of each data segment
clear xgrid
% [ if a frequency-dependent correction is being applied to the signal,  
%   e.g. frequency-dependent hydrophone sensitivity, it should be applied 
%   here to each frequency bin of the DFT ]


%% Compute power spectrum (EQUATION 8)

P = (X./N).^2;                      %power spectrum = square of amplitude                                  
clear X

%% Compute single-sided power spectrum (EQUATION 9)

%Pss = 2*P(:,10:floor(N/2)+1);        %remove DC (0 Hz) component and 
                                    % frequencies above Nyquist frequency
                                    % Fs/2 (index of Fs/2 = N/2+1), divide
                                    % by noise power bandwidth
                                    %JW: I CHANGED THE LOW BIN FROM 2 TO 10
                                    %TO BE SURE TO REMOVE DC OFFSET ON
                                    %SOUNDTRAP STARTUP
                                    
                                    % xxxx KJP edit - I changed it back
                                    %because I think there are better ways of
                                    % accounting for this and it's kludging my 
                                    % dynamic frequency limits system. Must revisitxxxx
Pss = 2*P(:,2:floor(N/2)+1);
clear P
%% Compute frequencies of DFT bins

f = floor(Fs/2)*linspace(1/(N/2),1,N/2);
                                    %calculate frequencies of DFT bins
flow = find(single(f) >= lcut,1,'first');   %low-frequency cut-off                                    
fhigh = find(single(f) <= hcut,1,'last');   %high-frequency cut-off
f = f(flow:fhigh);                  %frequency bins in user-defined range
nf = length(f);                     %number of frequency bins

%% Compute noise power bandwidth and delta(f)

B = (1/N).*(sum((w/alpha).^2));     %noise power bandwidth (EQUATION 12)
delf = Fs/N;                        %frequency bin width

%% Convert to dB

switch atype                  
    case 'PSD'                      %if PSD selected (EQUATION 11)
        a = 10*log10((1/(delf*B))*Pss(:,flow:fhigh)./(pref^2))-S;  
    case 'PowerSpec'                %if power spectrum selected
        a = 10*log10(Pss(:,flow:fhigh)./(pref^2))-S;  %EQUATION 10
    case 'Broadband'                %if broadband level selected
        %a = 10*log10((1/B)*sum(Pss(:,flow:fhigh),2)./(pref^2))-S;
        a = 10*log10(sum(Pss(:,flow:fhigh),2)./(pref^2))-S;
                                    %EQUATION 17

%% 1/3 octave analysis (if selected)
    case 'TOL'
% Generate 1/3-octave frequencies
    if lcut < 25
        lcut = 25;
    end
    lobandf = floor(log10(lcut));   %lowest power of 10 frequency for 1/3 
                                    % octave band computation
    hibandf = ceil(log10(hcut));    %highest ""
    nband = 10*(hibandf-lobandf)+1; %number of 1/3-octave bands
    fc = zeros(1,nband);            %initialise 1/3-octave frequency vector
    fc(1) = 10^lobandf;             %lowest frequency = lowest power of 10

% Calculate centre frequencies (corresponds to EQUATION 13)        
    
    for i = 2:nband                 %calculate 1/3 octave centre 
        fc(i) = fc(i-1)*10^0.1;     % frequencies to (at least) precision 
    end                             % of ANSI standard
    
    fc = fc(find(fc >= lcut,1,'first'):find(fc <= hcut,1,'last'));
                                    %crop frequency vector to frequency
                                    %   range of data

    nfc = length(fc);               %number of 1/3 octave bands
    
% Calculate boundary frequencies of each band (EQUATIONS 14-15)    
    fb = fc*10^-0.05;               %lower bounds of 1/3 octave bands
    fb(nfc+1) = fc(nfc)*10^0.05;    %upper bound of highest band (upper
                                    %   bounds of previous bands are lower
                                    %   bounds of next band up in freq.)
    if max(fb) > hcut               %if highest 1/3 octave band extends 
        nfc = nfc-1;                %   above highest frequency in DFT, 
        fc = fc(1:nfc);             %   remove highest band
    end
    

% Calculate 1/3-octave band levels (corresponds to EQUATION 16)
    P13 = zeros(M,nfc);             %initialise TOL array
        
    for i = 1:nfc                   %loop through centre frequencies
        fli = find(f >= fb(i),1,'first');   %index of lower bound of band
        fui = find(f < fb(i+1),1,'last');   %index of upper bound of band
        for q = 1:M                 %loop through DFTs of data segments
            fcl = sum(Pss(q,fli:fui));%integrate over mth band frequencies
            P13(q,i) = fcl ;         %store TOL of each data segment
        end
    end
    if ~isempty(P13(1,10*log10(P13(1,:)/(pref^2)) <= -10^6))
        lowcut = find(10*log10(P13(1,:)/(pref^2)) <= -10^6,1,'last') + 1;
                                    %index lowest band before empty bands
                                    % at low frequencies
        P13 = P13(:,lowcut:nfc);        %remove empty low-frequency bands
        fb = fb(lowcut:nfc+1);      
        fc = fc(lowcut:nfc);
        nfc = length(fc);              %redefine nfc
    end
	a = 10*log10((1/B)*P13/(pref^2))-S; %TOLs
    clear P13
end


%% Calculations for case All

switch atype   
    case 'All'
        %find cutoff frequencies for decade bands
        f10 = find(single(f) >= 10,1,'first');   %10 Hz
        f100 = find(single(f) >= 100,1,'first');   %100 Hz
        f1000 = find(single(f) >= 1000,1,'first');   %1000 Hz
        f10000 = find(single(f) >= 10000,1,'first');   %10000 Hz
        f100000 = find(single(f) <= 100000,1,'last');   %100000 Hz
        
        %calculate levels
        
        % KJP comment out xxx
        %changed to specific freq as Lcut is being set to 25 during 1/3...
        % oct calcs and then used thereafter
        %apsd = 10*log10((1/(delf*B))*Pss(:,f10:f100000)./(pref^2))-S;   
        % end kjp edits xxx
        
        % KJP uncomment xxx
        apsd = 10*log10((1/(delf*B))*Pss(:,flow:fhigh)./(pref^2))-S;  
        % end kjp uncomment xxx
        
        %apowspec = 10*log10(Pss(:,flow:fhigh)./(pref^2))-S;  %EQUATION 10
        %a = 10*log10((1/B)*sum(Pss(:,flow:fhigh),2)./(pref^2))-S;
        abroad = 10*log10(sum(Pss(:,f10:f100000),2)./(pref^2))-S; %ditto comment above re 1/3 oct
        %abroad = 10*log10(sum(Pss(:,flow:fhigh),2)./(pref^2))-S; %EQUATION 17
        
        
        a10_100 = 10*log10(sum(Pss(:,f10:f100),2)./(pref^2))-S;
        a100_1000 = 10*log10(sum(Pss(:,f100:f1000),2)./(pref^2))-S;
        a1000_10000 = 10*log10(sum(Pss(:,f1000:f10000),2)./(pref^2))-S;
        a10000_100000 = 10*log10(sum(Pss(:,f10000:f100000),2)./(pref^2))-S;
        

% Generate 1/3-octave frequencies
    if lcut < 25
        lcut = 25;
    end
    lobandf = floor(log10(lcut));   %lowest power of 10 frequency for 1/3 
                                    % octave band computation
    hibandf = ceil(log10(hcut));    %highest ""
    nband = 10*(hibandf-lobandf)+1; %number of 1/3-octave bands
    fc = zeros(1,nband);            %initialise 1/3-octave frequency vector
    fc(1) = 10^lobandf;             %lowest frequency = lowest power of 10

% Calculate centre frequencies (corresponds to EQUATION 13)        
    
    for i = 2:nband                 %calculate 1/3 octave centre 
        fc(i) = fc(i-1)*10^0.1;     % frequencies to (at least) precision 
    end                             % of ANSI standard
    
    fc = fc(find(fc >= lcut,1,'first'):find(fc <= hcut,1,'last'));
                                    %crop frequency vector to frequency
                                    %   range of data

    nfc = length(fc);               %number of 1/3 octave bands
    
% Calculate boundary frequencies of each band (EQUATIONS 14-15)    
    fb = fc*10^-0.05;               %lower bounds of 1/3 octave bands
    fb(nfc+1) = fc(nfc)*10^0.05;    %upper bound of highest band (upper
                                    %   bounds of previous bands are lower
                                    %   bounds of next band up in freq.)
    if max(fb) > hcut               %if highest 1/3 octave band extends 
        nfc = nfc-1;                %   above highest frequency in DFT, 
        fc = fc(1:nfc);             %   remove highest band
    end
    

% Calculate 1/3-octave band levels (corresponds to EQUATION 16)
    P13 = zeros(M,nfc);             %initialise TOL array
        
    for i = 1:nfc                   %loop through centre frequencies
        fli = find(f >= fb(i),1,'first');   %index of lower bound of band
        fui = find(f < fb(i+1),1,'last');   %index of upper bound of band
        for q = 1:M                 %loop through DFTs of data segments
            fcl = sum(Pss(q,fli:fui));%integrate over mth band frequencies
            P13(q,i) = fcl ;         %store TOL of each data segment
        end
    end
    if ~isempty(P13(1,10*log10(P13(1,:)/(pref^2)) <= -10^6))
        lowcut = find(10*log10(P13(1,:)/(pref^2)) <= -10^6,1,'last') + 1;
                                    %index lowest band before empty bands
                                    % at low frequencies
        P13 = P13(:,lowcut:nfc);        %remove empty low-frequency bands
        fb = fb(lowcut:nfc+1);      
        fc = fc(lowcut:nfc);
        nfc = length(fc);              %redefine nfc
    end
	a13 = 10*log10((1/B)*P13/(pref^2))-S; %TOLs
    clear P13
end
clear Pss


%% Compute time vector

tint = (1-r)*N/Fs;                  %time interval in secs between segments
ttot = M*tint-tint;                 %total duration of file in seconds
t = 0:tint:ttot;                    %time vector in seconds   
if ~isempty(tstamp)                 %time stamp data if selected
    t = tstamp + datenum(0,0,0,0,0,t);
end

%% Construct output array
apsd = double(apsd);
abroad = double(abroad);
a10_100 = double(a10_100);
a100_1000 = double(a100_1000);
a1000_10000 = double(a1000_10000);
a10000_100000 = double(a10000_100000);
a13 = double(a13);


%PSD
APSD = zeros(M+1,nf+1);
APSD(2:M+1,2:nf+1) = apsd;
APSD(1,2:nf+1) = f; 
APSD(2:M+1,1) = t;

%broadband
ABROAD = [t.',abroad];
ABROAD = [zeros(1,2);ABROAD];

%decade band
A10_100 = [t.',a10_100];
A10_100 = [zeros(1,2);A10_100];

A100_1000 = [t.',a100_1000];
A100_1000 = [zeros(1,2);A100_1000];

A1000_10000 = [t.',a1000_10000];
A1000_10000 = [zeros(1,2);A1000_10000];

A10000_100000 = [t.',a10000_100000];
A10000_100000 = [zeros(1,2);A10000_100000];


%TOL
A13 = zeros(M+1,nfc+1);
A13(2:M+1,2:nfc+1) = a13;
A13(1,2:nfc+1) = fc; 
A13(2:M+1,1) = t;
f = fc;


tock = toc;
if disppar == 1,fprintf([' done in ' num2str(tock) ' s.\n']),end