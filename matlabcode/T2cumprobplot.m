function T2cumprobplot(data1, data2, data3, data4, data5, data6, data7)
%creates a cumulative probabilty plot of received levels
%data1 is the RL for 20-1,000 Hz
%data2 is the RL for 1,000-96,000 Hz
%Jason Wood Aug 2012

%% 2 input data sets
if nargin == 2

    % generate bins for the x axis


% now plot the data
x = floor(min(data1)):ceil(max(data1));
[n1,x] = hist(data1,x);
prob1 = n1/size(data1,2);
plot(x,cumsum(prob1), 'Linewidth', 3);

hold on;

x = floor(min(data2)):ceil(max(data2));
[n2,x] = hist(data2,x);
prob2 = n2/size(data2,2);
plot(x,cumsum(prob2),'r', 'Linewidth', 3);
ylim([0 1]);

set(gca,'Fontsize', 14, 'Fontweight', 'b');
ylabel('Cumulative Probability', 'Fontsize',14, 'Fontweight', 'b');
xlabel('Sound Pressure Level (dB re 1\muPa)', 'Fontsize',14, 'Fontweight', 'b');
set(gca,'YTick',[0:0.1:1]);
grid 'on';

legend('0.02-1 kHz','1-96 kHz', 'Location','SouthEast');

%% 1 input data set
elseif nargin ==1
% generate bins for the x axis
x = floor(min(data1)):ceil(max(data1));

% now plot the data
[n1,x] = hist(data1,x);
prob1 = n1/size(data1,1);
plot(x,cumsum(prob1), 'Linewidth', 3);
ylim([0 1]);

set(gca,'Fontsize', 14, 'Fontweight', 'b');
ylabel('Cumulative Probability', 'Fontsize',14, 'Fontweight', 'b');
xlabel('Sound Pressure Level (dB re 1\muPa)', 'Fontsize',14, 'Fontweight', 'b');
set(gca,'YTick',[0:0.1:1]);
grid 'on';


%% 7 inputs
elseif nargin == 7
    
    % generate bins for the x axis
%    xmax(i,1) = max(['data' i]);
 %   xmin(i,1) = min(['data' i]);
%end
%x = floor(min(xmin)):ceil(max(xmax));

% now plot the data
x = floor(min(data1)):ceil(max(data1));
[n1,x] = hist(data1,x);
prob1 = n1/size(data1,2);
plot(x,cumsum(prob1), 'k', 'Linewidth', 3);

hold on;

x = floor(min(data2)):ceil(max(data2));
[n2,x] = hist(data2,x);
prob2 = n2/size(data2,2);
plot(x,cumsum(prob2),'r', 'Linewidth', 3);

x = floor(min(data3)):ceil(max(data3));
[n3,x] = hist(data3,x);
prob3 = n3/size(data3,2);
plot(x,cumsum(prob3),'c', 'Linewidth', 3);

x = floor(min(data4)):ceil(max(data4));
[n4,x] = hist(data4,x);
prob4 = n4/size(data4,2);
plot(x,cumsum(prob4),'b', 'Linewidth', 3);

x = floor(min(data5)):ceil(max(data5));
[n5,x] = hist(data5,x);
prob5 = n5/size(data5,2);
plot(x,cumsum(prob5),'m', 'Linewidth', 3);

x = floor(min(data6)):ceil(max(data6));
[n6,x] = hist(data6,x);
prob6 = n6/size(data6,2);
plot(x,cumsum(prob6),'--k', 'Linewidth', 3);

x = floor(min(data7)):ceil(max(data7));
[n7,x] = hist(data7,x);
prob7 = n7/size(data7,2);
plot(x,cumsum(prob7),':k', 'Linewidth', 3);

ylim([0 1]);
xlim([75 135]);

set(gca,'Fontsize', 14, 'Fontweight', 'b');
ylabel('Cumulative Probability', 'Fontsize',14, 'Fontweight', 'b');
xlabel('Sound Pressure Level (dB re 1\muPa)', 'Fontsize',14, 'Fontweight', 'b');
set(gca,'YTick',[0:0.1:1]);
grid 'on';

legend('0.02-96 kHz','0.02-0.1 kHz','0.1-1 kHz', '1-10 kHz', '10-96 kHz', '0.5-15 kHz', '15-40 kHz', 'Location','SouthEast');


%% 4 inputs

elseif nargin == 4
    
    % generate bins for the x axis
%    xmax(i,1) = max(['data' i]);
 %   xmin(i,1) = min(['data' i]);
%end
%x = floor(min(xmin)):ceil(max(xmax));

% now plot the data
x = floor(min(data1)):ceil(max(data1));
[n1,x] = hist(data1,x);
prob1 = n1/size(data1,1);
plot(x,cumsum(prob1), 'm', 'Linewidth', 3);

hold on;

x = floor(min(data2)):ceil(max(data2));
[n2,x] = hist(data2,x);
prob2 = n2/size(data2,1);
plot(x,cumsum(prob2),'c', 'Linewidth', 3);

x = floor(min(data3)):ceil(max(data3));
[n3,x] = hist(data3,x);
prob3 = n3/size(data3,1);
plot(x,cumsum(prob3),'k', 'Linewidth', 3);

x = floor(min(data4)):ceil(max(data4));
[n4,x] = hist(data4,x);
prob4 = n4/size(data4,1);
plot(x,cumsum(prob4),'r', 'Linewidth', 3);

ylim([0 1]);
xlim([80 140]);

set(gca,'Fontsize', 14, 'Fontweight', 'b');
ylabel('Cumulative Probability', 'Fontsize',14, 'Fontweight', 'b');
xlabel('Sound Pressure Level (dB re 1\muPa)', 'Fontsize',14, 'Fontweight', 'b');
set(gca,'YTick',[0:0.1:1]);
grid 'on';

legend('No beluga or boats','Boats only','Beluga only', 'Boats and beluga', 'Location','SouthEast');


%% 5 inputs

elseif nargin == 5
    
    % generate bins for the x axis
%    xmax(i,1) = max(['data' i]);
 %   xmin(i,1) = min(['data' i]);
%end
%x = floor(min(xmin)):ceil(max(xmax));

% now plot the data
x = floor(min(data1)):ceil(max(data1));
[n1,x] = hist(data1,x);
prob1 = n1/size(data1,2);
plot(x,cumsum(prob1), 'm', 'Linewidth', 3);

hold on;

x = floor(min(data2)):ceil(max(data2));
[n2,x] = hist(data2,x);
prob2 = n2/size(data2,2);
plot(x,cumsum(prob2),'c', 'Linewidth', 3);

x = floor(min(data3)):ceil(max(data3));
[n3,x] = hist(data3,x);
prob3 = n3/size(data3,2);
plot(x,cumsum(prob3),'k', 'Linewidth', 3);

x = floor(min(data4)):ceil(max(data4));
[n4,x] = hist(data4,x);
prob4 = n4/size(data4,2);
plot(x,cumsum(prob4),'r', 'Linewidth', 3);

x = floor(min(data5)):ceil(max(data5));
[n5,x] = hist(data5,x);
prob5 = n5/size(data5,2);
plot(x,cumsum(prob5),'b', 'Linewidth', 3);
ylim([0 1]);
xlim([75 135]);

set(gca,'Fontsize', 14, 'Fontweight', 'b');
ylabel('Cumulative Probability', 'Fontsize',14, 'Fontweight', 'b');
xlabel('Sound Pressure Level (dB re 1\muPa)', 'Fontsize',14, 'Fontweight', 'b');
set(gca,'YTick',[0:0.1:1]);
grid 'on';

legend('SRKW','Ships','Boats','Depth Sounders', 'Remainder', 'Location','NorthWest');

end
