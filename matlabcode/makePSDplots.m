% Make the mother fucking PSD distribution plots
% Based of JW code, based off NM paper
% Kaitlin Palmer 2020-06-20 kjp@smruconsulting.com

function makePSDplots(appObj, data)

% Create the title name

% Create the time bounds for the axis limits
switch appObj.plotScale
    case 'Year'
    titleStr = ['Noise level statistics for '...
        datestr(data.time(2), 'yyyy')];

    
    fname = [datestr(data.time(2), 'yyyy'), '_PSD'];
    case 'Month'
        

    titleStr = ['Noise level statistics for '...
        datestr(data.time(2), 'mmmm yyyy')];
        
    fname = [datestr(data.time(2), 'yyyymm'), '_PSD'];
    case 'Day'
        
    titleStr = ['Noise level statistics for '...
        datestr(data.time(2), 'mmm-dd-yyyy')]
    
    fname = [datestr(data.time(2), 'yyyymmdd'), '_PSD'];
    
end


% Adjust the datetime if adjust for local toggle has been selected

if appObj.AdjustForLocalCheckBox.Value
    
    disp(['Converting UTC to ' appObj.LocalAreaDropDown.Value 'or '...
        appObj.LocalTimeDropDown.Value ' hrs  with '...
        'daylight savings'])
    
    
    datetimeAdj = datetime(data.time,'TimeZone','UTC');
    
    % Use the GMT offset to find the timezone
    datetimeAdj.TimeZone = appObj.LocalAreaDropDown.Value;

end


% Compute stats
tic
PSD = data.PSD(2:end,:);
fint = data.meta.ff(6)- data.meta.ff(5);


%%% VECTORIZE ALL THIS SHIT %%%
% %EQUATION 18
% %memory not sufficient to do rms and p and min/maxdB all at once.
% %RMSlevel = 10*log10(mean(10.^(PSD/10))); %calculate RMS Level
% RMSlevel = zeros(1,size(PSD,2));    %prealocate for speed
% p = zeros(99,size(PSD,2));      %prealocate for speed
% tempmin = zeros(1,size(PSD,2));    %prealocate for speed
% tempmax = zeros(1,size(PSD,2));    %prealocate for speed
% for i = 1:size(PSD,2)
%     p(:,i) = prctile(PSD(:,i),1:99).';
%     tempmin(1,i) = min(PSD(:,i));
%     tempmax(1,i) = max(PSD(:,i));
% end
% p = p'; %rotate p matrix
%
% mindB = floor(min(tempmin)/10)*10;
% maxdB = ceil(max(tempmax)/10)*10;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% That's better! %

% Calculate the RMS level as a function of frequency
RMSlevel = 10*log10(mean(10.^(PSD/10),1));

% Get the percentiles
pBands = [5,25,50,75,95];
p = prctile(PSD, pBands,1)'; % that function was totally coppied out of a toolbox


% %minimum dB level rounded down to nearest 10
% Calculate the maximum and min levels
mindB = floor(min(PSD(:))/10)*10;
maxdB = ceil(max(PSD(:))/10)*10;


% Compute SPD if more than 1000 data points in time domain
hind = 0.1;             %histogram bin width for probability densities (PD)

%dB values at which to calculate empirical PD (comments go above the code)
dbvec = mindB:hind:maxdB;




%EQUATION 19

M = size(PSD,1)-1; % Not sure the point of this
d = hist(PSD,dbvec)/(hind*(M));         %SPD array

d(d == 0) = NaN;                        %suppress plotting of empty hist bins
nf = length(data.meta.ff);
d = [d d(:,nf)];                        %add dummy column for highest frequency
[X,Y] = meshgrid([data.meta.ff,data.meta.ff(nf)]-fint/2,dbvec);

tock = toc;

fprintf('Plotting...')

tic

fig1 = figure(113),clf                             %initialise figure
set(fig1, 'Position', [100, 100, 1050, 740]);
set(fig1,'color','w')
hold off

set(gca,'XScale','log')
g = pcolor(X,Y,d);                      %SPD
set(g,'LineStyle','none')

hold on
cvals =[...
    {[0 0 0]};...
    {[0.1 0.1 0.1]};...
    {[0.2 0.2 0.2]};...
    {[0.3 0.3 0.3]};...
    {[0.4 0.4 0.4]}];
h =semilogx(data.meta.ff,p','linewidth',2);   %percentiles
set(h, {'color'}, cvals);
semilogx(data.meta.ff,RMSlevel,'m','linewidth',2)   %RMS Level


try load('wenz.mat')
    semilogx(fwenz,wenz(1,:), ':k','linewidth',2)
    semilogx(fwenz,wenz(2,:), ':k','linewidth',2)
catch
    disp('Wenz curves not found')
end


%semilogx(fPSD,RMSlevel,'m','linewidth',2)   %RMS Level
set(gca,'XScale','log','TickDir','out','layer','top','fontsize',14,'fontname','Arial')
grid on;
title(titleStr,'interpreter','none')
xlabel('Frequency (Hz)')
ylabel('PSD (dB re 1 \muPa^2/Hz)')

caxis([0 0.05])
ylabel(colorbar('southoutside'),'Empirical Probability Density','fontsize',14,'fontname','Arial')
[a, ~, ~, ~] = legend('SPD','L5','L25','L50','L75','L95','Leq','Wenz','location','northeast');
a.LineWidth = 1;    %deal with figure legend box not showing

ylim([min(mindB, min(wenz(:))) max(maxdB, max(wenz(:)))]);
xlim([min(data.meta.ff) max(data.meta.ff)])



%xlabel('Frequency (Hz)')
set(gca,'xticklabel',[])
ylabel('SPL (dB re 1 \muPa )');

% Make sure the folder exists
mkdir(appObj.plotPath);

% Match figure standards
fnameout = fullfile(appObj.plotPath, [fname, '.png']);

% Save the figure
print(fig1,fnameout,'-dpng','-r200');    




tock = toc;

fprintf([' done in ' num2str(tock) ' s.\n'])




end