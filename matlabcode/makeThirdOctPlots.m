% Based of JW code, based off NM paper
% Kaitlin Palmer 2020-06-20 kjp@smruconsulting.com

function makeThirdOctPlots(appObj, data)
%% 1/3 Oct plot



% Create the time bounds for the axis limits
switch appObj.plotScale
    case 'Year'
    titleStr = ['1/3 Octave levels for '...
        datestr(data.time(2), 'yyyy')];
    
    fname = [datestr(data.time(2), 'yyyy'), '_1_3Oct']

    case 'Month'
        

    titleStr = ['1/3 Octave levels for '...
        datestr(data.time(2), 'mmmm yyyy')];
        
    fname = [datestr(data.time(2), 'yyyymm'), '_1_3Oct'];
    case 'Day'
        
    titleStr = ['1/3 Octave levels for '...
        datestr(data.time(2), 'mmm-dd-yyyy')];
    fname = [datestr(data.time(2), 'yyyymmdd'), '_1_3Oct'];
    
end

thirdOct = data.thirdOct;

% check if timestamps are stored with the third otave data (new) or
% not(old)

if thirdOct(1) == 0 || thirdOct(1)>7e5
    thirdOct = data.thirdOct(2:end, 2:end);
end

f13 = data.thirdOct(1,2:end);     %store the 1/3 oct freq bins

RMSlevel = 10*log10(mean(10.^(thirdOct/10))); %calculate RMS Level

p = prctile(thirdOct,1:99);   %percentiles

Octmin = min(thirdOct,[],1);   %min
Octmax = max(thirdOct,[],1);   %max

fig1=figure(114),clf                             %initialise figure
set(fig1, 'Position', [100, 100, 1050, 300]);
set(fig1,'color','w')
hold off

% This could be made prettier.... but I would like to pretend to have a
% life for a bit
plot(f13,RMSlevel,'r','linewidth',2);   %plot the rms levels
hold on

% Simulate divisor
divisor = round(exp(linspace(.25, 1.75, length(f13)))*14);
% Not really sure what this is doing, simulated in more flexible way above
%divisor = [20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 42 45 48 51 54 57 60 63 66 69 72 75 78];
width = log10(f13).*(f13./divisor); %help with constant bar width on log scale

for i = 1:size(thirdOct,2)
    plot([f13(1,i) f13(1,i)],[Octmin(1,i) Octmax(1,i)], 'k',  'linewidth',2);   %plot the vertical min-max
    patch([f13(1,i)-width(1,i) f13(1,i)+width(1,i) f13(1,i)+width(1,i) f13(1,i)-width(1,i)], [p(75,i) p(75,i) p(25,i) p(25,i)],'w', 'linewidth',2); %plot 25 to 75 percentile plots
    plot([f13(1,i)-width(1,i) f13(1,i)+width(1,i)],[p(95,i) p(95,i)], 'k', 'linewidth',2);  %plot the 5 percentile
    plot([f13(1,i)-width(1,i) f13(1,i)+width(1,i)],[p(50,i) p(50,i)], 'k:', 'linewidth',2);  %plot the median
    plot([f13(1,i)-width(1,i) f13(1,i)+width(1,i)],[p(5,i) p(5,i)], 'k', 'linewidth',2);  %plot the 95 percentile
end
set(gca,'XScale','log','TickDir','in','layer','top','fontsize',14,'fontname','Arial')
grid on;


xlabel('Frequency (Hz)')
%set(gca,'xticklabel',[])
ylabel('SPL (dB re 1 \muPa )');

% Match figure standards
ax = gca
ax.XAxis.FontSize = 12;
ax.YAxis.FontSize = 14;
t = title(titleStr,'interpreter','none')
t.FontSize =10


set(fig1, 'units','inch','position',[0,0,15,5]);
fnameout = fullfile(appObj.plotPath, [fname, '.png']);

 mkdir(appObj.plotPath)

% Save the figure
print(fig1,fnameout,'-dpng','-r200');    


end