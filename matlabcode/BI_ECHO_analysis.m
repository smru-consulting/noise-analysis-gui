%Runs analyses for Burrard Inlet 2020 noise project using Merchant 2015 code
%averages in 1-minute bins
    
%Original code Jason Wood May 2019
%the PG_FuncJWdcoff.m file this calls up has been changed to remove the 1st
%second of each wav file since the ST has a large initial DC offset (line 242) 
%I also changed the PG_DFTJW.m file to remove more of the DC offset (line %106)

%% input variables that are set for every run

%set path and files to analyze
path = ['Z:\Projects\2020 Burrard Inlet\Feb-May2020\671113255\Decompressed\202005'];    %folder to analyze
outpath = ['Z:\Projects\2020 Burrard Inlet\Feb-May2020\STAnalyses\Results\671113255\'];
files = dir(['Z:\Projects\2020 Burrard Inlet\Feb-May2020\671113255\Decompressed\202005\*.wav']);    %create a list of wav files in this directory
tstring = '----------yymmddHHMMSS.wav';       %set the format to read time from filename 470065216.190129113301.wav
disppar = 'All';        %will display that are doing all analyses
atype = 'All';      %will run all analyses
plottype = 'None';  %no plots
envi = 'Wat';       %uses under water dB reference
calib = 1;          %yes, is calibrated
ctype = 'EE';       %end to end calibration

disp('blarg')

%SI used for deployment 1
%Si = -188.7;        %end to end sensitivty of SoundTrap # 671113255 Site 4
%Si = -189;        %end to end sensitivty of SoundTrap # 470302784 Spare
%Si = -188.3;        %end to end sensitivty of SoundTrap # 671129639 Site 3
%Si = -188.3;        %end to end sensitivty of SoundTrap # 671109159 Site 1
%Si = -187.9;        %end to end sensitivty of SoundTrap # 470065216 Site 2
%SI used for deployment 2
%Si = -175.5;        %end to end sensitivty of SoundTrap # 671129639 Site 3
%Si = -175.6;        %end to end sensitivty of SoundTrap # 470065216 Site 2
%Si = -175.9;        %end to end sensitivty of SoundTrap # 470302784 Site 5
%Si = -175.7;        %end to end sensitivty of SoundTrap # 671109159 Site 1
%Si = -175.4;        %end to end sensitivty of SoundTrap # 671113255 Site 4
%SI used for deployment 3
%because we weren't sure on the quality of the calibration, I used the
%manufacturers sensitivity settings for this deployment
%Si = -176.2;        %end to end sensitivty o f SoundTrap # 671129639 Site 3
% Si = -188.8;        %end to end sensitivty of SoundTrap # 470065216 Site 2
% Si = -176.5;        %end to end sensitivty of SoundTrap # 470302784 Site 5
% Si = -188.9;        %end to end sensitivty of SoundTrap # 671109159 Site 1
% Si = -176.2;        %end to end sensitivty of SoundTrap # 671113255 Site 4
%SI used for deployment 4
%Si =  -175.5;        %end to end sensitivty o f SoundTrap # 671129639 Site 3
% Si = -187.9;        %end to end sensitivty of SoundTrap # 470065216 Site 2
% Si =    ;        %end to end sensitivty of SoundTrap # 470302784 Site 5
%  Si = -188;        %end to end sensitivty of SoundTrap # 671109159 Site 1
% Si =  -175.7;        %end to end sensitivty of SoundTrap # 671113255 Site 4

%SI used for Feb-May 2020 Deployment
%Si =  -175.5;        %end to end sensitivty o f SoundTrap # 671129639 BE03
Si =  -175.7;        %end to end sensitivty of SoundTrap # 671113255 EB05

channel = 1;        %if is stereo wav file, select which channel
r = 0.5;            %window overlap
Fs = 96000;        %Fs
N = 96000;         %FFT size
winname = 'Hann';   %window type
lcut = 10;          %10 Hz low cut freq
hcut = 48000;      %48 kHz high cut freq
calstring = 'Abs';  %needed for PAMGuide batch loop
welch = 60*(Fs/N)/(1-r);  %ratio of new to original window lengths in Welch method (avg over 60 secs)
linlog = 0;         %assume this is for plotting and this is log scale
writeout = 0;       %supresses csv output
Mh = [];            %mic sensitivity filler 
G = [];             %gain setting filler
vADC = [];          %v 0 to P filler
chunksize = [];     %if reading parts of larger file
plottype = 'None'   %supress plot
batch = 1;          %sets files to contiguous (i.e. each wav file in contiguous)


% MORE USELESS COMMENTS!!!%

% PLEASE REMEMBER TO COMMENT YOUR CODE!!!% 
%% PAMGuide batch loop

%path = get(handles.browser,'userdata');
        metadir = ['PAMGuide_Batch_' atype '_' calstring '_' num2str(N)...
            'pt' winname 'Window' '_' num2str(r*100) 'pcOlap'];
        batch = 1;
        if writeout == 1,mkdir(path,metadir);end
        %files = get(handles.RUN,'UserData');
        nf = length(files);
        disp(['No. of audio files in selected directory: ' num2str(nf)])
        tic;
        tall = tic;
        for i = 1:nf
            if i == 1,disppar = 1;else disppar = 0;end
            tic
            [APSD,ABROAD,A10_100,A100_1000,...
                A1000_10000,A10000_100000,A13,f]  =...
                PG_FuncJWdcoff(files(i).name, path, atype, plottype, envi,...
                calib, ctype, Fs, Si, Mh, G, vADC, r, N, winname, lcut, hcut,...
                tstring,metadir,writeout,disppar,welch,chunksize,batch,...
                linlog,channel);
            if i == 1                   %initialise concatenated array on first iteration
                conkAPSD = APSD;
                conkABROAD = ABROAD;
                conkA10_100 = A10_100;
                conkA100_1000 = A100_1000;
                conkA1000_10000 = A1000_10000;
                conkA10000_100000 = A10000_100000;
                conkA13 = A13;
                                
            elseif length(conkAPSD(1,:)) == length(APSD(1,:))
                [raPSD,~] = size(APSD);
                %if isempty(tstring);tdiff = conkAPSD(3,1)-conkAPSD(2,1);APSD(2:raPSD,1) = APSD(2:raPSD,1)+conkAPSD(length(conkAPSD(:,1)),1)+tdiff;end
                conkAPSD = [conkAPSD; APSD(2:raPSD,:)];
                clear APSD
            %elseif length(conkABROAD(1,:)) == length(ABROAD(1,:))
                [raBROAD,~] = size(ABROAD);
                %if isempty(tstring);tdiff = conkABROAD(3,1)-conkABROAD(2,1);ABROAD(2:raBROAD,1) = ABROAD(2:raBROAD,1)+conkABROAD(length(conkABROAD(:,1)),1)+tdiff;end
                conkABROAD = [conkABROAD; ABROAD(2:raBROAD,:)];
                clear ABROAD
            %elseif length(conkA10_100(1,:)) == length(A10_100(1,:))
                [ra10_100,~] = size(A10_100);
                %if isempty(tstring);tdiff = conkA10_100(3,1)-conkA10_100(2,1);A10_100(2:ra10_100,1) = A10_100(2:ra10_100,1)+conkA10_100(length(conkA10_100(:,1)),1)+tdiff;end
                conkA10_100 = [conkA10_100; A10_100(2:ra10_100,:)];
                clear A10_100
            %elseif length(conkA100_1000(1,:)) == length(A100_1000(1,:))
                [ra100_1000,~] = size(A100_1000);
                %if isempty(tstring);tdiff = conkA100_1000(3,1)-conkA100_1000(2,1);A100_1000(2:ra100_1000,1) = A100_1000(2:ra100_1000,1)+conkA100_1000(length(conkA100_1000(:,1)),1)+tdiff;end
                conkA100_1000 = [conkA100_1000; A100_1000(2:ra100_1000,:)];
                clear A100_1000
            %elseif length(conkA1000_10000(1,:)) == length(A1000_10000(1,:))
                [ra1000_10000,~] = size(A1000_10000);
                %if isempty(tstring);tdiff = conkA1000_10000(3,1)-conkA1000_10000(2,1);A1000_10000(2:ra1000_10000,1) = A1000_10000(2:ra1000_10000,1)+conkA1000_10000(length(conkA1000_10000(:,1)),1)+tdiff;end
                conkA1000_10000 = [conkA1000_10000; A1000_10000(2:ra1000_10000,:)];
                clear A1000_10000
           %elseif length(conkA10000_100000(1,:)) == length(A10000_100000(1,:))
                [ra10000_100000,~] = size(A10000_100000);
                %if isempty(tstring);tdiff = conkA10000_100000(3,1)-conkA10000_100000(2,1);A10000_100000(2:ra10000_100000,1) = A10000_100000(2:ra10000_100000,1)+conkA10000_100000(length(conkA10000_100000(:,1)),1)+tdiff;end
                conkA10000_100000 = [conkA10000_100000; A10000_100000(2:ra10000_100000,:)];
                clear A10000_100000
             %elseif length(conkA13(1,:)) == length(A13(1,:))
                [ra13,~] = size(A13);
                %if isempty(tstring);tdiff = conkA13(3,1)-conkA13(2,1);A13(2:ra13,1) = A13(2:ra13,1)+conkA13(length(conkA13(:,1)),1)+tdiff;end
                conkA13 = [conkA13; A13(2:ra13,:)];
                clear A13
            
            else
                disp('Sample rates of files not equal. Concatenation aborted.')
            end
            tock = toc;
            fprintf(['File ' num2str(i) '/' num2str(nf) ': ' files(i).name ' analysed in ' num2str(tock) ' s\n'])
        end
        tockall = toc(tall);
        disp(['Analysis complete in ' num2str(tockall) ' s.'])
        
        %ofile = ['PAMGuide_Batch_' atype '_' calstring '_' num2str(N) 'pt' winname 'Window' '_' num2str(r*100) 'pcOlap.csv'];
        %PG_Viewer(conk,plottype,ofile,linlog)
        %encode A(1,1) with analysis metadata
        %aid = 0;
        %switch atype
            %case 'PSD',aid = aid + 1;
            %case 'PowerSpec',aid = aid + 2;
            %case 'TOL',aid = aid + 3;
            %case 'Broadband',aid = aid + 4;
            %case 'Waveform',aid = aid + 5;
            %case 'TOLf',aid = aid + 3;
        %end
        %if calib == 1,aid = aid + 10;else aid = aid + 20;end
        %if strcmp(envi,'Air'), aid = aid + 100;else aid = aid + 200;end
        %if tstick == 1, aid = aid + 1000;else aid = aid + 2000;end
        %conk(1,1) = aid;
        %fprintf('Writing concatenated output array...'),tic
        %if tstick == 1
            %dlmwrite(fullfile(path,ofile),conk,'precision',15,'delimiter',',');
        %else
            %dlmwrite(fullfile(path,ofile),conk,'precision',9,'delimiter',',');
        %end
        tock = toc; 
        fprintf(['done in ' num2str(tock) ' s.\n'])


%% split the results by UTC day and then save in the outpath folder

startday = floor(conkABROAD(2,1));  %first day of data
endday = ceil(conkABROAD(end,1));  %last day of data

days = (startday:endday)'; %vector of days

%change names of files so can save them using old name
A10000_100000 = conkA10000_100000;
A1000_10000 = conkA1000_10000;
A100_1000 = conkA100_1000;
A10_100 = conkA10_100; 
A13 = conkA13;
ABROAD = conkABROAD;
APSD = conkAPSD;
clearvars conkAPSD   %remove psd since so large

for i = 1:size(days,1)-1
    
    %create a name of file for each day
    [Y,M,D] = datevec(days(i,1));
  
    if M < 10 && D > 9
        dayfile = [num2str(Y) '0' num2str(M) num2str(D)];
    elseif M < 10 && D < 10
        dayfile = [num2str(Y) '0' num2str(M) '0' num2str(D)];
    elseif M > 9 && D < 10
        dayfile = [num2str(Y) num2str(M) '0' num2str(D)];  
    else
        dayfile = [num2str(Y) num2str(M) num2str(D)];
    end

    
    %find the data for that UTC day
    index = find(ABROAD(:,1) >= days(i,1) & ABROAD(:,1) < days(i+1,1));
    conkA10000_100000 = A10000_100000(1,:); %store first row
    conkA10000_100000(2:size(index)+1,:) = A10000_100000(index,:);  %store the rest of the data
    conkA1000_10000 = A1000_10000(1,:); %store first row
    conkA1000_10000(2:size(index)+1,:) = A1000_10000(index,:);  %store the rest of the data
    conkA100_1000 = A100_1000(1,:); %store first row
    conkA100_1000(2:size(index)+1,:) = A100_1000(index,:);  %store the rest of the data
    conkA10_100 = A10_100(1,:); %store first row
    conkA10_100(2:size(index)+1,:) = A10_100(index,:);  %store the rest of the data
    conkA13 = A13(1,:); %store first row
    conkA13(2:size(index)+1,:) = A13(index,:);  %store the rest of the data
    conkABROAD = ABROAD(1,:); %store first row
    conkABROAD(2:size(index)+1,:) = ABROAD(index,:);  %store the rest of the data
    conkAPSD = APSD(1,:); %store first row
    conkAPSD(2:size(index)+1,:) = APSD(index,:);  %store the rest of the data
    
    
    save([outpath dayfile '.mat'], 'conkA10000_100000', 'conkA1000_10000', 'conkA100_1000', 'conkA10_100', 'conkA13', 'conkABROAD', 'conkAPSD');
end

clearvars
