 %plot the Burrard Inlet noise data for reporting
%Jason Wood April 2016

%for Burrard Inlet the months are Calender months
load('LunarMonths2020.mat');  %load the months. THESE ARE NOT LUNAR MONTHS BUT CALENDAR MONTHS

t = datetime(2020,01,01); 
%select which lunar month to start with (0 = Jan, 1 = Feb, 2 = Mar, 3 = April, 4 = May)
Monthnum = month(t);
MonthTitle = month(t, 'name');
YearTitle = year(t);
starttime = LunarMonths(Monthnum);   %starting time starttime = LunarMonths(Monthnum,1);
startdate = floor(starttime);       %round down for date
endtime = LunarMonths(Monthnum+1); %endtime = LunarMonths(Monthnum+1,1);
enddate = ceil(endtime);        %round up for date


%CHANGE BASED ON WHICH LOCATION ANALYZING
% infolder = 'Z:\Projects\2019 Burrard Inlet\Noise analyses\Results\470065216';
% infolder = 'Z:\Projects\2019 Burrard Inlet\Noise analyses\Results\671109159';
% infolder = 'Z:\Projects\2019 Burrard Inlet\Noise analyses\Results\671113255';%indian arm
% infolder = 'Z:\Projects\2019 Burrard Inlet\Noise analyses\Results\671129639';
infolder = 'Z:\Projects\2020 Burrard Inlet\Feb-May2020\STAnalyses\Results\671113255';
infolder = 'C:\Users\Kaitlin Palmer\Desktop\NL GUI test data\OldOutputTest';
% outfolder = 'Z:\Projects\2019 Burrard Inlet\Noise analyses\Results\Month\470065216\Month1';
% outfolder = 'Z:\Projects\2019 Burrard Inlet\Noise analyses\Results\Month\671109159\Month1';%change month number here
% outfolder = 'Z:\Projects\2019 Burrard Inlet\Noise analyses\Results\Month\671113255\Month1';
% outfolder = 'Z:\Projects\2019 Burrard Inlet\Noise analyses\Results\Month\671129639\Month1';
outfolder = 'Z:\Projects\2020 Burrard Inlet\Feb-May2020\STAnalyses\Results\671113255\January';


dayfiles = dir([infolder '\*.mat']);

filedate = zeros(size(dayfiles,1),1);
    
for i = 1:size(dayfiles,1)  
    Y = str2num(dayfiles(i).name(1:4));  %year
    M = str2num(dayfiles(i).name(5:6));  %month
    D = str2num(dayfiles(i).name(7:8));  %day
    filedate(i,1) = datenum(Y,M,D);  %save datenum of file
end

%% load in data for the lunar month
fprintf('Loading the data for the lunar month...')
tic

begin = find(filedate >= startdate & filedate < enddate,1, 'first');    %first day folder in lunar month
finish = find(filedate >= startdate & filedate < enddate,1, 'last');    %last day folder in lunar month
Lunarfiles = dayfiles(begin:finish);   %all files in lunar month

%create blank matrix
t = [];
SPL = [];   
PSD = [];
thirdOct = [];

%load a file to store frequency bins
load([infolder '\' Lunarfiles(1).name]);


fPSD = conkAPSD(1,2:end);   %store the PSD freq bins
f13 = conkA13(1,2:end);     %store the 1/3 oct freq bins

for i = 1:size(Lunarfiles,1)
    load([infolder '\' Lunarfiles(i).name]);   %load the data for that day
    
    %deal with partial first and last day
    if i == 1 || i == size(Lunarfiles,1)
        firstdata = find(conkABROAD >= starttime & conkABROAD < endtime, 1, 'first');   
        lastdata = find(conkABROAD >= starttime & conkABROAD < endtime, 1, 'last'); 
        timetemp = conkABROAD(firstdata:lastdata,1);    %store the times of measurements
        temp = conkABROAD(firstdata:lastdata,2);  %store the SPL data
        temp(:,2) = conkA10_100(firstdata:lastdata,2);  %store the decade data
        temp(:,3) = conkA100_1000(firstdata:lastdata,2);  %store the decade data
        temp(:,4) = conkA1000_10000(firstdata:lastdata,2);  %store the decade data
        temp(:,5) = conkA10000_100000(firstdata:lastdata,2);  %store the decade data
        PSDtemp = conkAPSD(firstdata:lastdata,2:end);   %store the PSD data
        thirdtemp = conkA13(firstdata:lastdata,2:end);  %store the 1/3 oct data
    else
        timetemp = conkABROAD(2:end,1); %store the times of measurements
        temp = conkABROAD(2:end,2);  %store the SPL data
        temp(:,2) = conkA10_100(2:end,2);  %store the decade data
        temp(:,3) = conkA100_1000(2:end,2);  %store the decade data
        temp(:,4) = conkA1000_10000(2:end,2);  %store the decade data
        temp(:,5) = conkA10000_100000(2:end,2);  %store the decade data
        PSDtemp = conkAPSD(2:end,2:end);   %store the PSD data
        thirdtemp = conkA13(2:end,2:end);  %store the 1/3 oct data
    end
    
    t = [t; timetemp];  %concatenate the time data
    SPL = [SPL; temp];  %concatenate the data
    PSD = [PSD; PSDtemp];   %concatenate the data
    thirdOct = [thirdOct; thirdtemp];   %concatenate the data
    clear timetemp temp PSDtemp thirdtemp
end
% 
% save([outfolder '\LunarMonth' num2str(Monthnum) 'SPL'], 'SPL', 'starttime', 'startdate', 'endtime', 'enddate', 't');  %save SPL data
% 
% save([outfolder '\LunarMonth' num2str(Monthnum) 'PSD'], 'PSD', 'starttime', 'startdate', 'endtime', 'enddate', 't', 'fPSD', '-v7.3');  %save PSD data
% 
% save([outfolder '\LunarMonth' num2str(Monthnum) 'thirdOct'], 'thirdOct', 'starttime', 'startdate', 'endtime', 'enddate', 't', 'f13');  %save 1/3 oct data

%clearvars
tock = toc;

fprintf([' done in ' num2str(tock) ' s.\n'])

%% broadband and decade band plot with spectrogram
%TIMES FOR SPECGRAM ARE PLOTTED IN UTC!!!!

%Monthnum = 7;   %change this!
%load(['T:\smru-ftp\Projects\ECHO LK noise budget\analysis\Lunar\LunarMonth' num2str(Monthnum) 'SPL.mat']);    %load the SPL data

%load(['T:\smru-ftp\Projects\ECHO LK noise budget\analysis\Lunar\LunarMonth' num2str(Monthnum) 'aPSD.mat']);    %load the PSD data
%load(['T:\smru-ftp\Projects\ECHO LK noise budget\analysis\Lunar\LunarMonth' num2str(Monthnum) 'bPSD.mat']);    %load the PSD data
%load(['T:\smru-ftp\Projects\ECHO LK noise budget\analysis\Lunar\LunarMonth' num2str(Monthnum) 'cPSD.mat']);    %load the PSD data
%load(['T:\smru-ftp\Projects\ECHO LK noise budget\analysis\Lunar\LunarMonth' num2str(Monthnum) 'dPSD.mat']);    %load the PSD data
%PSD = [PSD1; PSD2; PSD3; PSD4]; %concatenate PSD
%clear PSD1 PSD2 PSD3 PSD4;

%consolidate into 1 hour resolution
onelookup = 0:1/24:1;  %generate decimals at 1 hour intervals
onelookup = onelookup';
temp1 = starttime - startdate;    %find the fraction of the day for the start
oneindex1 = find(onelookup <= temp1,1,'last'); %find the nearest floor 1 hour
temp2 = abs(fix(endtime) - endtime);    %find the fraction of the day for the start
oneindex2 = find(onelookup >= temp2,1,'first'); %find the nearest ceiling 1 hour
onehour = startdate+onelookup(oneindex1,1):1/24:fix(endtime)+onelookup(oneindex2,1);  %generate the start times for each 1 hour period.
onehour = onehour';
onehour = onehour(1:end-1,1); %shorten length by one

SPLonehour = NaN(size(onehour,1),5);    %set size for speed
PSDonehour = NaN(size(onehour,1),size(PSD,2));  %set size for speed
for i = 1:size(onehour,1)
    index = find(t(:,1)>= onehour(i,1) & t(:,1) < onehour(i,1)+(1/24));  %find data during time period
    %SPLonehour(i,1) = onehour(i,1);    %record the start of the half hour of data
    if size(index > 0)
        temp = SPL(index,:);
        SPLonehour(i,:) = 10*log10(mean(10.^(temp./10)));
        tempPSD = 10.^(PSD(index,:)./10);
        PSDonehour(i,:) = 10*log10(mean(tempPSD,1));        
    end
    clear temp tempPSD
end

% save([outfolder '\LunarMonth' num2str(Monthnum) 'PSDonehour'], 'PSDonehour', 'starttime', 'startdate', 'endtime', 'enddate', 't', 'fPSD', '-v7.3');  %save PSD data
% 
% save([outfolder '\LunarMonth' num2str(Monthnum) 'SPLonehour'], 'SPLonehour', 'starttime', 'startdate', 'endtime', 'enddate', 't');  %save SPL data


%clear PSD SPL;


%plot the data
figure(111),clf                             %initialise figure
set(figure(111), 'Position', [100, 100, 1050, 525]);
set(figure(111),'color','w')
hold off

plot(onehour(:,1),SPLonehour(:,1), 'color', [0,0,0]+0.5, 'LineWidth',2);  %plot broadband in gray
hold on
plot(onehour(:,1),SPLonehour(:,2), 'b', 'LineWidth',2);  %plot 10-100 Hz in blue
plot(onehour(:,1),SPLonehour(:,3), 'r', 'LineWidth',2);  %plot 100-1000 Hz in read
plot(onehour(:,1),SPLonehour(:,4), 'g', 'LineWidth',2);  %plot 1000-10000 Hz in green
plot(onehour(:,1),SPLonehour(:,5), 'm', 'LineWidth',2);  %plot 10000-100000 Hz in magenta
hold off

set(gca,'tickdir','out','layer','top','fontname','arial','fontsize',14);
xlim([min(onehour(:,1)) max(onehour(:,1))]);
ylim([70 150]);%actually [90 150]
% ylim([70 140]);    %for Indian Arm[80 130]
ylabel(['rms SPL (dB re 1 \muPa)']); 
title(['Sound Pressure Level for ' MonthTitle ' ' YearTitle]); 
dateFormat = 2;     %set date format to m/d/y
datetick('x',dateFormat,'keeplimits');
grid on;
grid minor;
[a, ~, ~, ~] = legend('0.01-100','0.01-0.1','0.1-1','1-10','10-100 kHz','Location','northoutside','Orientation','horizontal');
a.LineWidth = 1;    %deal with figure legend box not showing
set(gca,'xticklabel',[])


figure(112),clf                             %initialise figure
set(figure(112), 'Position', [100, 100, 1050, 525]);
set(figure(112),'color','w') 
hold off

fint = fPSD(3) - fPSD(2);
% surf(onehour(:,1),[fPSD-fint/2 max(fPSD)+fint/2],[PSDonehour.'],'EdgeColor','none');
surf(onehour(:,1),fPSD,PSDonehour.','EdgeColor','none');
colormap('jet');
set(gca,'YScale','log');
set(gca,'tickdir','out','layer','top','fontname','arial','fontsize',14);
ylim([min(fPSD)-fint/2 max(fPSD)+fint/2]);    
xlim([min(onehour(:,1)) max(onehour(:,1))]);
ylabel('Frequency (Hz)')
ylabel(colorbar('southoutside'),['PSD (dB re 1 \muPa^2/Hz )'],'fontname','arial','fontsize',14)
view(0,90)
caxis([0 150])
%load(['T:\smru-ftp\Projects\ECHO LK noise budget\analysis\Lunar\LunarMont
dateFormat = 2;     %set date format to m/d/y
datetick('x',dateFormat,'keeplimits', 'keepticks');
%set(gcf,'Renderer','Zbuffer')  %helps deal with lack of plot for low
%values but takes a long time and does litle for the final look of the
%figure

%clearvars;

%% PSD plots
%%%MOVE THIS SECTION UP SO CAN REMOVE PSD FILE SOONER!
%Monthnum = 7;   %change this!
%load(['T:\smru-ftp\Projects\ECHO LK noise budget\analysis\Lunar\LunarMonth' num2str(Monthnum) 'aPSD.mat']);    %load the PSD data
%load(['T:\smru-ftp\Projects\ECHO LK noise budget\analysis\Lunar\LunarMonth' num2str(Monthnum) 'bPSD.mat']);    %load the PSD data
%load(['T:\smru-ftp\Projects\ECHO LK noise budget\analysis\Lunar\LunarMonth' num2str(Monthnum) 'cPSD.mat']);    %load the PSD datah' num2str(Monthnum) 'dPSD.mat']);    %load the PSD data
%PSD = [PSD1; PSD2; PSD3; PSD4]; %concatenate PSD
%clear PSD1 PSD2 PSD3 PSD4;
%load(['T:\smru-ftp\Projects\ECHO LK noise budget\analysis\Lunar\LunarMonth' num2str(Monthnum) 'PSD.mat']);    %load the PSD data

% Compute stats
tic
M = size(PSD,1)-1;
fint = fPSD(3) - fPSD(2);

%EQUATION 18
%memory not sufficient to do rms and p and min/maxdB all at once.
%RMSlevel = 10*log10(mean(10.^(PSD/10))); %calculate RMS Level
RMSlevel = zeros(1,size(PSD,2));    %prealocate for speed
p = zeros(99,size(PSD,2));      %prealocate for speed
tempmin = zeros(1,size(PSD,2));    %prealocate for speed
tempmax = zeros(1,size(PSD,2));    %prealocate for speed
for i = 1:size(PSD,2)
    RMSlevel(1,i) = 10*log10(mean(10.^(PSD(:,i)/10)));
    p(:,i) = prctile(PSD(:,i),1:99).'; 
    tempmin(1,i) = min(PSD(:,i));
    tempmax(1,i) = max(PSD(:,i));
end
p = p'; %rotate p matrix
%p = prctile(PSD,1:99).';                %prctile is in the MATLAB Statistics Toolbox. For those without this toolbox, an alternative (to be placed in same folder as PAMGuide.m) can be downloaded here: http://users.powernet.co.uk/kienzle/octave/matcompat/scripts/statistics/prctile.m  

mindB = floor(min(tempmin)/10)*10;
%maxdB = floor(max(tempmax)/10)*10;      %CONSIDER SETTING THIS AT 130 TO AVOID CUTOFF AT 120 IN THE PSD PLOT!!!! OR SET THIS TO CEILING INSTEAD OF FLOOR!!!
maxdB = ceil(max(tempmax)/10)*10;
%maxdB = 130;

clear tempmin tempmax
%mindB = floor(min(min(PSD(PSD>-Inf)))/10)*10;
                        %minimum dB level rounded down to nearest 10
%maxdB = ceil(max(max(PSD(PSD<Inf)))/10)*10;
                        %maximum dB level rounded up to nearest 10

% Compute SPD if more than 1000 data points in time domain

%if ~strcmp(atype,'Broadband') && M>1000

hind = 0.1;             %histogram bin width for probability densities (PD)
dbvec = mindB:hind:maxdB;
                        %dB values at which to calculate empirical PD




%EQUATION 19

d = hist(PSD,dbvec)/(hind*(M));         %SPD array

d(d == 0) = NaN;                        %suppress plotting of empty hist bins
nf = length(fPSD);
d = [d d(:,nf)];                        %add dummy column for highest frequency

%switch atype
    %case {'PSD';'PowerSpec'}           %axis array for SPD pcolor plot
        [X,Y] = meshgrid([fPSD,fPSD(nf)]-fint/2,dbvec);
    %case {'TOL';'TOLf'} 
        %[X,Y] = meshgrid([f,f(nf)*10^0.05]*10^-0.05,dbvec);
%end
%end

tock = toc;

fprintf([' done in ' num2str(tock) ' s.\n'])

%save the above files that are needed for PSD plots so don't have to
%recalculate again....
% save([outfolder '\LunarMonth' num2str(Monthnum) 'PSDplotVar.mat'], 'fPSD','RMSlevel','p','X','Y','d');


% Plot
load('Z:\Projects\2019 Burrard Inlet\Noise analyses\matlabcodes\wenz.mat');  
%load the upper and lower bounds of Wenz curve
%if aren't rerunning the PSD code above, load the psd data you need.
%load(['T:\smru-ftp\Projects\ECHO LK noise budget\analysis\Lunar\LunarMonth' num2str(Monthnum) 'PSDplotVar.mat']);

fprintf('Plotting...')

tic

figure(113),clf                             %initialise figure
set(figure(113), 'Position', [100, 100, 1050, 740]);
set(figure(113),'color','w')
hold off

set(gca,'XScale','log')
g = pcolor(X,Y,d);                      %SPD
set(g,'LineStyle','none')

hold on

semilogx(fPSD,p(:,95),'k','linewidth',2)   %percentiles
semilogx(fPSD,p(:,75),'color',[0.1 0.1 0.1],'linewidth',2)
semilogx(fPSD,p(:,50),'color',[0.2 0.2 0.2],'linewidth',2)
semilogx(fPSD,p(:,25),'color',[0.3 0.3 0.3],'linewidth',2)
semilogx(fPSD,p(:,5),'color',[0.4 0.4 0.4],'linewidth',2)
semilogx(fPSD,RMSlevel,'m','linewidth',2)   %RMS Level
semilogx(fwenz,wenz(1,:), ':k','linewidth',2)
semilogx(fwenz,wenz(2,:), ':k','linewidth',2)

%semilogx(fPSD,RMSlevel,'m','linewidth',2)   %RMS Level
set(gca,'XScale','log','TickDir','out','layer','top','fontsize',14,'fontname','Arial')
grid on;
title(['Noise level statistics for ' MonthTitle ' ' YearTitle],'interpreter','none')
xlabel('Frequency (Hz)')
ylabel('PSD (dB re 1 \muPa^2/Hz)')


%if ~strcmp(atype,'Broadband') && M>1000
caxis([0 0.05])
ylabel(colorbar('southoutside'),'Empirical Probability Density','fontsize',14,'fontname','Arial')
%legend('SPD','L5','L25','L50','L75','L95','Leq','Wenz','location','southwest')
[a, ~, ~, ~] = legend('SPD','L5','L25','L50','L75','L95','Leq','Wenz','location','northeast');
a.LineWidth = 1;    %deal with figure legend box not showing
%else
    %legend('99%','95%','50%','5%','1%','RMS Level','location','southwest')
%end

ylim([20 150]);
%ylim([mindB maxdB])
xlim([min(fPSD) max(fPSD)])

tock = toc;

    fprintf([' done in ' num2str(tock) ' s.\n'])

clearvars -except outfolder;

%% 1/3 Oct plot
%Monthnum = 1;  %change this!
load([outfolder '\LunarMonth' num2str(Monthnum) 'thirdOct.mat']);    %load the 1/3 Oct data

RMSlevel = 10*log10(mean(10.^(thirdOct/10))); %calculate RMS Level

p = prctile(thirdOct,1:99);   %percentiles

Octmin = min(thirdOct,[],1);   %min
Octmax = max(thirdOct,[],1);   %max

figure(114),clf                             %initialise figure
set(figure(114), 'Position', [100, 100, 1050, 300]);
set(figure(114),'color','w')
hold off

plot(f13,RMSlevel,'r','linewidth',2);   %plot the rms levels
hold on
divisor = [20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 42 45 48 51 54 57 60 63 66 69 72 75 78];
width = log10(f13).*(f13./divisor); %help with constant bar width on log scale

for i = 1:size(thirdOct,2)
    plot([f13(1,i) f13(1,i)],[Octmin(1,i) Octmax(1,i)], 'k',  'linewidth',2);   %plot the vertical min-max
    patch([f13(1,i)-width(1,i) f13(1,i)+width(1,i) f13(1,i)+width(1,i) f13(1,i)-width(1,i)], [p(75,i) p(75,i) p(25,i) p(25,i)],'w', 'linewidth',2); %plot 25 to 75 percentile plots
    plot([f13(1,i)-width(1,i) f13(1,i)+width(1,i)],[p(95,i) p(95,i)], 'k', 'linewidth',2);  %plot the 5 percentile
    plot([f13(1,i)-width(1,i) f13(1,i)+width(1,i)],[p(50,i) p(50,i)], 'k:', 'linewidth',2);  %plot the median
    plot([f13(1,i)-width(1,i) f13(1,i)+width(1,i)],[p(5,i) p(5,i)], 'k', 'linewidth',2);  %plot the 95 percentile
end
set(gca,'XScale','log','TickDir','in','layer','top','fontsize',14,'fontname','Arial')
grid on;

title(['1/3 Octave levels for' MonthTitle ' ' YearTitle],'interpreter','none')
%xlabel('Frequency (Hz)')
set(gca,'xticklabel',[])
ylabel(['SPL (dB re 1 \muPa )']);


clearvars -except outfolder;

%% broadband and 1/3 oct table

%Monthnum = 1;  %change this!
load([outfolder '\LunarMonth' num2str(Monthnum) 'SPL.mat']);    %load the SPL data
load([outfolder '\LunarMonth' num2str(Monthnum) 'thirdOct.mat']);    %load the 1/3 Oct data

poct = prctile(thirdOct,1:99);   % 1/3 oct percentiles
pbroad = prctile(SPL(:,1),1:99);    %broardband percentiles

table = zeros(size(f13,2)+1,5); %set size of table
%broadband levels
table(1,1) = pbroad(1,5);   
table(1,2) = pbroad(1,25);
table(1,3) = pbroad(1,50);
table(1,4) = pbroad(1,75);
table(1,5) = pbroad(1,95);

%1/3 oct levels
table(2:end,1) = poct(5,:).';
table(2:end,2) = poct(25,:).';
table(2:end,3) = poct(50,:).';
table(2:end,4) = poct(75,:).';
table(2:end,5) = poct(95,:).';

%export the table 
csvwrite([outfolder '\LunarMonth' num2str(Monthnum) 'oct.csv'],table);

clearvars -except outfolder;

%% Daily and weekly plots

%Monthnum = 1;  %change this!
load([outfolder '\LunarMonth' num2str(Monthnum) 'SPL.mat']);    %load the SPL data

%NEED TO CHANGE THIS TO DEAL WITH TIME3 OFFSET MORE EFFECTIVELY, ESPECIALLY FOR TIMES THAT SPAN TIME CHANGES!!!
deal with UTC to local time offset
tlocal = zeros(size(t));

% these are time changes from/to PDT but in UTC (years 2018 through to
% 2020)
timechange1 = datenum(2018,3,11,10,00,00);
timechange2 = datenum(2018,11,4,9,00,00);
timechange3 = datenum(2019,3,10,10,00,00);
timechange4 = datenum(2019,11,3,9,00,00);
timechange5 = datenum(2020,3,8,10,00,00);% added in the year 2020
timechange6 = datenum(2020,11,1,9,00,00);

index = find(t<timechange1);
if size(index,1)>0
   tlocal(index,1) = t(index,1)-(8/24);
end
index = find(t>=timechange1 & t<timechange2);
if size(index,1)>0
   tlocal(index,1) = t(index,1)-(7/24);
end
index = find(t>=timechange2 & t<timechange3);
if size(index,1)>0
    tlocal(index,1) = t(index,1)-(8/24);
end
index = find(t>=timechange3 & t<timechange4);
if size(index,1)>0
    tlocal(index,1) = t(index,1)-(7/24);
end
index = find(t>=timechange4 & t<timechange5);
if size(index,1)>0
    tlocal(index,1) = t(index,1)-(8/24);
end
index = find(t>=timechange5 & t<timechange6);
if size(index,1)>0
    tlocal(index,1) = t(index,1)-(7/24);
end

%tlocal = t; %ST data are in local not UTC!!!! Burrard Inlet AUG-NOV 2019
% is in UTC so comment this line for that and run lines earlier to this

ttemp = datenum_round_off(tlocal,'minute','round'); %round the minutes to deal with PAMGuard recordings
%dayindex = find(rem(ttemp,1) == 0);    %find the start of each day
uniquedays = unique(floor(ttemp));
dayindex = zeros(size(uniquedays,1),1);
for i = 1:size(uniquedays,1)
    dayindex(i,1) = find(floor(ttemp(:,1)) == uniquedays(i,1),1, 'first');
end

sundayindex = find(weekday(ttemp) == 1);    %find the day of the week (Sun = 1)
%sundayindex = find(weekday(tlocal) == 1);    %find the day of the week (Sun = 1)
%find the start of each week and day
    
k = 1;
for i = 1:size(dayindex,1)-1
    if dayindex(i)+719 <= size(SPL,1)
        SPLday(:,:,i) = SPL(dayindex(i):dayindex(i)+719,:);
    else
        %SPLday(:,:,i) = SPL(dayindex(i):end,:); Since this results in
        %wrong size, just ignore partial day of data
    end
    tempindex = find(sundayindex == dayindex(i,1));
    if size(tempindex > 0)
        weekindex(k,1) = dayindex(i,1);
        k = k + 1;
    end
end

for i = 1:size(weekindex,1)-1
    SPLweek(:,:,i) = SPL(weekindex(i):weekindex(i)+(720*7)-1,:);
end

    
SPLdaymedian = median(SPLday,3);
daytime = 1/720:1/720:1;
SPLweekmedian= median(SPLweek,3);

%plot the daily data
figure(115),clf                             %initialise figure
set(figure(115), 'Position', [100, 100, 1050, 525]);
set(figure(115),'color','w')
hold off

plot(daytime,SPLdaymedian(:,1), 'color', [0,0,0]+0.5, 'LineWidth',2);  %plot broadband in gray
hold on
plot(daytime,SPLdaymedian(:,2), 'b', 'LineWidth',2);  %plot 10-100 Hz in blue
plot(daytime,SPLdaymedian(:,3), 'r', 'LineWidth',2);  %plot 100-1000 Hz in read
plot(daytime,SPLdaymedian(:,4), 'g', 'LineWidth',2);  %plot 1000-10000 Hz in green
plot(daytime,SPLdaymedian(:,5), 'm', 'LineWidth',2);  %plot 10000-100000 Hz in magenta
hold off

set(gca,'tickdir','out','layer','top','fontname','arial','fontsize',14);
xlim([min(daytime) max(daytime)]);
ylim([70 150]);%actually [90 150]
% ylim([70 130]);    %for Indian Arm[70 120]
ylabel(['Median rms SPL (dB re 1 \muPa)']);
set(gca, 'XTick', (240/1440:240/1440:1339/1440));
set(gca, 'XTickLabel', ['04:00'; '08:00'; '12:00'; '16:00'; '20:00']);
xlabel(['Time Of Day (HH:MM, Local Time)']);
grid on;
[a, ~, ~, ~] = legend('0.01-100','0.01-0.1','0.1-1','1-10','10-100 kHz','Location','northoutside','Orientation','horizontal');
a.LineWidth = 1;    %deal with figure legend box not showing



%plot the weekly data
figure(116),clf                             %initialise figure
set(figure(116), 'Position', [100, 100, 1050, 525]);
set(figure(116),'color','w')
hold off

plot(SPLweekmedian(:,1), 'color', [0,0,0]+0.5, 'LineWidth',2);  %plot broadband in gray
hold on
plot(SPLweekmedian(:,2), 'b', 'LineWidth',2);  %plot 10-100 Hz in blue
plot(SPLweekmedian(:,3), 'r', 'LineWidth',2);  %plot 100-1000 Hz in read
plot(SPLweekmedian(:,4), 'g', 'LineWidth',2);  %plot 1000-10000 Hz in green
plot(SPLweekmedian(:,5), 'm', 'LineWidth',2);  %plot 10000-100000 Hz in magenta
hold off

set(gca,'tickdir','out','layer','top','fontname','arial','fontsize',14);
xlim([1 5040]);
ylim([70 150]);%actually [90 150]
% ylim([70 140]);    %for Indian Arm[80 120]
ylabel(['Median rms SPL (dB re 1 \muPa)']);
set(gca, 'XTick', (720:720:720*6));
set(gca, 'XTickLabel', ['Mon'; 'Tue'; 'Wed'; 'Thu'; 'Fri'; 'Sat']);
xlabel(['Day of Week (Local Time)']);
grid on;
[a, ~, ~, ~] = legend('0.01-100','0.01-0.1','0.1-1','1-10','10-100 kHz','Location','northoutside','Orientation','horizontal');
a.LineWidth = 1;    %deal with figure legend box not showing



%boxplot and table of SPL values

p = prctile(SPL,1:99);
clear RMSlevel; %otherwise for loop below just adds to previous RMSlevel

%rms levels
for i = 1:size(SPL,2)
    RMSlevel(1,i) = 10*log10(mean(10.^(SPL(:,i)/10)));
    SPLmin(1,i) = min(SPL(:,i));
    SPLmax(1,i) = max(SPL(:,i));
end

figure(117),clf                             %initialise figure
set(figure(117), 'Position', [100, 100, 600, 400]);
set(figure(117),'color','w')
hold off
x =  (1:5); %set dummy x variable for plotting
width = 0.25; %help with constant bar width on log scale
color = {'[0.5 0.5 0.5]'; '[0 0 1]'; '[1 0 0]'; '[0 1 0]'; '[1 0 1]'};

hold on
for i = 1:size(p,2)
    h(i) = plot([x(1,i) x(1,i)],[SPLmin(1,i) SPLmax(1,i)], 'color', cell2mat(color(i)),  'linewidth',2);   %plot the vertical min-max
    patch([x(1,i)-width x(1,i)+width x(1,i)+width x(1,i)-width], [p(75,i) p(75,i) p(25,i) p(25,i)],'w', 'EdgeColor', cell2mat(color(i)), 'linewidth',2); %plot 25 to 75 percentile plots
    plot([x(1,i)-width x(1,i)+width],[p(95,i) p(95,i)], 'color', cell2mat(color(i)), 'linewidth',2);  %plot the 5 percentile
    plot([x(1,i)-width x(1,i)+width],[p(50,i) p(50,i)], 'color', cell2mat(color(i)), 'linestyle', ':', 'linewidth',2);  %plot the median
    plot([x(1,i)-width x(1,i)+width],[p(5,i) p(5,i)], 'color', cell2mat(color(i)), 'linewidth',2);  %plot the 95 percentile
end
[a, ~, ~, ~] = legend([h(1) h(2) h(3) h(4) h(5)], '0.01-100','0.01-0.1','0.1-1','1-10','10-100 kHz','Location','northoutside','Orientation','horizontal');
plot(x,RMSlevel,'k','linewidth',2,'HandleVisibility','off');   %plot the rms levels
a.LineWidth = 1;    %deal with figure legend box not showing
set(gca,'XScale','lin','TickDir','in','layer','top','fontsize',14,'fontname','Arial')
grid on;

title(['1/3 Octave levels for ' MonthTitle ' ' YearTitle],'interpreter','none')
%xlabel('Frequency (Hz)')
set(gca,'xticklabel',[])
ylabel(['SPL (dB re 1 \muPa)'])
ylim([80 160]); %for indian arm [70 160] for Aug-Nov  otherwise [80 160]

%and the table
table = zeros(8,5);
table(1,:) = SPLmin;
table(2,:) = p(5,:);
table(3,:) = p(25,:);
table(4,:) = p(50,:);
table(5,:) = p(75,:);
table(6,:) = p(95,:);
table(7,:) = SPLmax;
table(8,:) = RMSlevel;

csvwrite([outfolder '\LunarMonth' num2str(Monthnum) 'SPL.csv'],table);

