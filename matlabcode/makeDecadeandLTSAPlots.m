% Based of JW code, based off NM paper
% Kaitlin Palmer 2020-06-20 kjp@smruconsulting.com


function makeDecadeandLTSAPlots(appObj, data)



% Max frequency bin
legendTxt = ['10-',num2str(min(100, data.meta.fs/2000)), ' kHz'];

% Create the time bounds for the axis limits
switch appObj.plotScale
    case 'Year'
        startVal = data.startDate;
        startVal.Month=1;
        startVal.Day =1;
        startVal.Hour =0;
        startVal.Minute =0;
        startVal.Second =.01;
        
        stopVal = startVal + calmonths(12) - seconds(1);
        fname = datestr(data.time(2), 'yyyy');
    case 'Month'
        
        
        startVal = data.startDate;
        startVal.Day =1;
        startVal.Hour =0;
        startVal.Minute =0;
        startVal.Second =.01;
        
        stopVal = startVal + calmonths(1) - seconds(1);
             fname =datestr(data.time(2), 'yyyymm');
    case 'Day'
        
        startVal = data.startDate;
        startVal.Hour =0;
        startVal.Minute =0;
        startVal.Second =.01;
        
        stopVal = startVal + days(1) - seconds(1);
        fname = datestr(data.time(2), 'yyyymmdd');
end


%% Aggregate the data on the hourly scale %%

dateStart = (floor(min(datenum(data.time(2:end)))));
dateStop = (ceil(max(datenum(data.time(2:end)))));

% Create an array of hours between the start and stop date
dataHrs = [dateStart:1/24:dateStop, dateStop + 1/24];

% Create the output
SPLonehour = nan(length(dataHrs)-1, 5);
PSDonehour = nan(length(dataHrs)-1,size(data.PSD,2));  %set size for speed

for ii =1:length(dataHrs)-1

    idx = find(datenum(data.time)>= dataHrs(ii)  & datenum(data.time)< dataHrs(ii+1));
    
    if ~isempty(idx)
        % Broadband
        SPLonehour(ii,:) = 10*log10(mean(10.^(data.SPL(idx,:)./10)));
        tempPSD = 10.^(data.PSD(idx,:)./10);
        PSDonehour(ii,:) = 10*log10(mean(tempPSD,1));
        
    end


end
% Ok this is getting hacky..
dataHrs = dataHrs(1:end-1);


%%


%plot the data
fig1 = figure(111),clf                             %initialise figure
set(fig1, 'Position', [100, 100, 1050, 525]);
set(fig1,'color','w')
hold off

% Set white as a dummy, change later
colorvals =  cellstr([ 'w', 'b', 'r', 'g', 'm']');
h = plot(dataHrs',SPLonehour', 'LineWidth',2); 

% Set he legacy colorvals then the gray one
set(h, {'color'}, (colorvals));
h(1).Color = [0 0 0]+.5;

% Add the legend
[a,~,~,~] = legend('0.01-100','0.01-0.1','0.1-1','1-10',legendTxt,...
    'Location','northoutside','Orientation','horizontal');




set(gca,'tickdir','out','layer','top','fontname','arial','fontsize',14);
xlim([datenum(startVal) datenum(stopVal)]);
ylim([70 150]);
ylabel('rms SPL (dB re 1 \muPa)'); 
title(['Sound Pressure Level for ' datestr(dateStart, 'mmm yyyy')]); 
grid on;
grid minor;
a.LineWidth = 1;    %deal with figure legend box not showing
set(gca,'xticklabel',[])


% Make sure the folder exists
mkdir(appObj.plotPath);

fnameOut = [fname, '_SPL.png'];

% Match figure standards
fnameout = fullfile(appObj.plotPath, fnameOut);

% Save the figure
print(fig1,fnameout,'-dpng','-r200');    


cmap = [[1 1 1]; jet(200)];

% I guess these plots go together

fig2 =figure(112),clf                             %initialise figure
set(gcf, 'Position', [100, 100, 1050, 525]);
set(gcf,'color','w') 
hold off

fint = data.meta.ff(3) - data.meta.ff(2);
surf(dataHrs',data.meta.ff',PSDonehour','EdgeColor','none');

colormap(cmap);
set(gca,'YScale','log');
set(gca,'tickdir','out','layer','top','fontname','arial','fontsize',14);
view(0,90)
ylim([min(data.meta.ff)-fint/2 max(data.meta.ff)+fint/2]);    
xlim([datenum(startVal) datenum(stopVal)]);
ylabel('Frequency (Hz)')
ylabel(colorbar('southoutside'),'PSD (dB re 1 \muPa^2/Hz )','fontname','arial','fontsize',14)

caxis([0 150])
%load(['T:\smru-ftp\Projects\ECHO LK noise budget\analysis\Lunar\LunarMont
dateFormat = 2;     %set date format to m/d/y
datetick('x',dateFormat,'keeplimits', 'keepticks');

% Make sure the folder exists
mkdir(appObj.plotPath);

fnameOut = [fname, '_PSD.png'];

% Match figure standards
fnameout = fullfile(appObj.plotPath, fnameOut);

% Save the figure
print(fig2,fnameout,'-dpng','-r200');    



end
